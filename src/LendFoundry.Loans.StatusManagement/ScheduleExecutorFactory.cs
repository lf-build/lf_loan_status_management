﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;

namespace LendFoundry.Loans.StatusManagement
{
    public class ScheduleExecutorFactory : IScheduleExecutorFactory
    {
        public IScheduleExecutor Create(ILoanService loanService, ILogger logger,
                                        IStatusManagementRepository repository, IEventHubClient eventHub)
            => new ScheduleExecutor(loanService, logger, repository, eventHub);

        public IScheduleExecutor Create(ILoanService loanService, ILogger logger, IEventHubClient eventHub)
            => new ScheduleExecutor(loanService, logger, eventHub);
    }
}
