﻿using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.StatusManagement
{
    public static class JobDispatcherExtenstions
    {
        public static void UseJobDispacher(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IJobDispatcher>().Start();
        }
    }
}
