﻿using System;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.EventHub.Client;

namespace LendFoundry.Loans.StatusManagement
{
    public class StatusChangeStrategyFactory : IStatusChangeStrategyFactory
    {
        public StatusChangeStrategyFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IStatusChangeStrategy Create(string loanReferenceNumber, string toStatus, DateTimeOffset? startDate = default(DateTimeOffset?), string fromStatus = "", DateTimeOffset? endDate = default(DateTimeOffset?))
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentException($"{nameof(loanReferenceNumber)} cannot be empty");

            if (string.IsNullOrWhiteSpace(toStatus))
                throw new ArgumentException($"{nameof(toStatus)} cannot be empty");

            var schedule = new Schedule(loanReferenceNumber, toStatus, startDate, fromStatus, endDate);
            var logger = Provider.GetService<ILogger>();
            var repository = Provider.GetService<IStatusManagementRepository>();
            var loanService = Provider.GetService<ILoanService>();
            var eventHub = Provider.GetService<IEventHubClient>();

            // When request does not have {startDate} and {endDate}, the status change should be 
            // executed immediately.
            if (!startDate.HasValue && !endDate.HasValue)
                return new ChangeImmediately(schedule, logger, repository, loanService, eventHub);

            // When request has {startDate} and {endDate} informed should be created two schedule entries, 
            // first entry related to when it should be applied and second entry related to when it 
            // should be reverted back.
            if (startDate.HasValue && endDate.HasValue)
                return new ScheduleWithExpireDate(schedule, logger, repository, eventHub);

            // When request has only {startDate} informed should be created one schedule entry
            if (startDate.HasValue && !endDate.HasValue)
                return new ScheduleStartDateOnly(schedule, logger, repository, eventHub);

            throw new Exception("Status change strategy could not be defined");
        }

        public IStatusChangeStrategy Create(string loanReferenceNumber, string scheduleId, string action)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentException($"{nameof(loanReferenceNumber)} cannot be empty");

            if (string.IsNullOrWhiteSpace(scheduleId))
                throw new ArgumentException($"{nameof(scheduleId)} cannot be empty");

            if (string.IsNullOrWhiteSpace(action))
                throw new ArgumentException($"{nameof(action)} cannot be empty");

            var schedule = new Schedule(loanReferenceNumber, scheduleId, action);
            var logger = Provider.GetService<ILogger>();
            var repository = Provider.GetService<IStatusManagementRepository>();
            var loanService = Provider.GetService<ILoanService>();
            var executorFactory = Provider.GetService<IScheduleExecutorFactory>();
            var eventHub = Provider.GetService<IEventHubClient>();

            return new ManualScheduleExecution(schedule, logger, repository, executorFactory, loanService, eventHub);
        }
    }
}
