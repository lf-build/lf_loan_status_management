﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Loans.StatusManagement.Jobs
{
    public class ClosePayoffCheckJob : JobContextValues, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            ExecuteAsync(() =>
            {
                ResolveContextValues(context);
                Logger.Info($"Task {nameof(ClosePayoffCheckJob)} started");

                var payoffCode = StatusCodes.PayOff;
                var loans = ScheduleService.GetAllPayOff();

                Logger.Info($"Total of {loans?.Count} loan(s) found Payoff to process");

                if (loans != null)
                    Parallel.ForEach(loans, referenceNumber =>
                    {
                        LoanService.UpdateLoanStatus(referenceNumber, payoffCode);
                    });

                Logger.Info($"Task {nameof(ClosePayoffCheckJob)} executed");
            });
        }
    }
}