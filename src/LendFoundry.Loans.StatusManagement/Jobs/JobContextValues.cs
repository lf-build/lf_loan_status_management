﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;
using Quartz;
using System;
using LendFoundry.EventHub.Client;

namespace LendFoundry.Loans.StatusManagement.Jobs
{
    public class JobContextValues
    {
        public void ResolveContextValues(IJobExecutionContext context)
        {
            var provider = context.MergedJobDataMap.Get("provider") as IServiceProvider;
            var reader = context.MergedJobDataMap.Get("tenantReader") as StaticTokenReader;

            Logger = provider.GetService<ILoggerFactory>().CreateLogger();
            Repository = provider.GetService<IStatusManagementRepositoryFactory>().Create(reader);
            LoanService = provider.GetService<ILoanServiceFactory>().Create(reader);
            ScheduleService = provider.GetService<IScheduleServiceFactory>().Create(reader);
            var configurationFactory = provider.GetService<IConfigurationServiceFactory>();
            TenantTime = provider.GetService<ITenantTimeFactory>().Create(configurationFactory, reader);
            ExecutorFactory = provider.GetService<IScheduleExecutorFactory>();
            ConfigurationKeys = configurationFactory.Create<ConfigurationKeys>(Settings.ServiceName, reader).Get();
            EventHub = provider.GetService<IEventHubClient>();
        }

        protected ConfigurationKeys ConfigurationKeys { get; set; }

        protected ILogger Logger { get; private set; }

        protected IStatusManagementRepository Repository { get; private set; }

        protected ILoanService LoanService { get; private set; }

        protected ITenantTime TenantTime { get; private set; }

        protected IScheduleExecutorFactory ExecutorFactory { get; private set; }

        protected IEventHubClient EventHub { get; private set; }

        protected IScheduleService ScheduleService { get; private set; }

        protected void ExecuteAsync(Action expression)
        {
            try
            {
                expression.Invoke();
            }
            catch (Exception ex)
            {
                Logger.Error("Unhandled exception while running Job:", ex);
            }
        }
    }
}
