﻿using Quartz;
using System;
using System.Threading.Tasks;

namespace LendFoundry.Loans.StatusManagement.Jobs
{
    public class DelinquentCheckJob : JobContextValues, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            ExecuteAsync(() =>
            {
                ResolveContextValues(context);
                Logger.Info($"Task {nameof(DelinquentCheckJob)} started");

                var delinquentCode = StatusCodes.Delinquent;
                var loans = ScheduleService.GetAllDelinquent();

                Logger.Info($"Total of {loans?.Count} loan(s) found to Delinquent process");

                if (loans != null)
                    Parallel.ForEach(loans, referenceNumber =>
                    {
                        var isDelinquentAlready = LoanService.GetStatus(referenceNumber).Code == delinquentCode;
                        if (!isDelinquentAlready)
                        {
                            // If context loan is delinquent and before it was not, change it to Delinquent status                        
                            LoanService.UpdateLoanStatus(referenceNumber, delinquentCode);
                        }
                    });

                Logger.Info($"Task {nameof(DelinquentCheckJob)} executed");
            });
        }
    }
}
