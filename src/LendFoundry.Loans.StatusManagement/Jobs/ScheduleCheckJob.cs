﻿using LendFoundry.Foundation.Date;
using Quartz;
using System.Linq;

namespace LendFoundry.Loans.StatusManagement.Jobs
{
    public class ScheduleCheckJob : JobContextValues, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            ExecuteAsync(() =>
            {
                ResolveContextValues(context);
                Logger.Info($"Task {nameof(ScheduleCheckJob)} started");
                Logger.Info("Checking for scheduled entries...");

                // Gets all schedules entries to be executed today.
                var todaySchedules = Repository.All(
                    q => q.Date.Day == new TimeBucket(TenantTime.Today).Day &&
                         q.Status == ScheduleState.Pending).Result;

                Logger.Info($"Total of {todaySchedules.Count()} schedule(s) found to be processed");

                // Execute all scheduled entries.
                var scheduleExecutor = ExecutorFactory.Create(LoanService, Logger, Repository, EventHub);
                todaySchedules
                    .ToList()
                    .ForEach(schedule =>
                    {
                        scheduleExecutor.RunIt(schedule);
                    });
                Logger.Info($"Task {nameof(ScheduleCheckJob)} executed");
            });
        }
    }
}
