﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using Microsoft.Framework.DependencyInjection;
using System;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Loans.StatusManagement
{
    public class StatusManagementServiceFactory : IStatusManagementServiceFactory
    {
        public StatusManagementServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IStatusManagementService Create(ILogger logger)
        {
            var strategyFactory = Provider.GetService<IStatusChangeStrategyFactory>();
            var loanService = Provider.GetService<ILoanService>();
            var tenantTime = Provider.GetService<ITenantTime>();
            var loanConfiguration = Provider.GetService<IConfigurationService<LoanConfiguration>>().Get();

            return new StatusManagementService(strategyFactory, logger, loanService, tenantTime, loanConfiguration);
        }
    }
}
