﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using System;

namespace LendFoundry.Loans.StatusManagement
{
    internal class BaseStrategy
    {
        protected BaseStrategy
        (
            Schedule schedule,
            ILogger logger,
            IStatusManagementRepository repository,
            IEventHubClient eventHub,
            ILoanService loanService = default(ILoanService)
        )
        {
            Schedule = schedule;
            Logger = logger;
            Repository = repository;
            LoanService = loanService;
            EventHub = eventHub;
        }

        protected IEventHubClient EventHub { get; }

        protected ILogger Logger { get; }

        protected Schedule Schedule { get; }

        protected IStatusManagementRepository Repository { get; }

        protected ILoanService LoanService { get; }

        protected string GenerateScheduleId() => Guid.NewGuid().ToString("N");

        protected IScheduleDetail GetFirstSchedulePeriod(string scheduleId) => new ScheduleDetail
        {
            Date = new TimeBucket(Schedule.StartDate.Value),
            LoanReferenceNumber = Schedule.LoanReferenceNumber,
            LoanStatusCode = Schedule.ToStatusCode,
            ScheduleId = scheduleId,
            Status = ScheduleState.Pending
        };

        protected IScheduleDetail GetLastSchedulePeriod(string scheduleId) => new ScheduleDetail
        {
            Date = new TimeBucket(Schedule.EndDate.Value),
            LoanReferenceNumber = Schedule.LoanReferenceNumber,
            LoanStatusCode = Schedule.FromStatusCode,
            ScheduleId = scheduleId,
            Status = ScheduleState.Pending
        };
    }
}
