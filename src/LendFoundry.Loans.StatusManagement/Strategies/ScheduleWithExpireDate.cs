﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using System;

namespace LendFoundry.Loans.StatusManagement
{
    internal class ScheduleWithExpireDate : BaseStrategy, IStatusChangeStrategy
    {
        public ScheduleWithExpireDate
        (
            Schedule schedule,
            ILogger logger,
            IStatusManagementRepository repository,
            IEventHubClient eventHub
        ) : base(schedule, logger, repository, eventHub)
        { }

        public void Execute()
        {
            try
            {
                var pairScheduleId = GenerateScheduleId();
                Repository.Add(GetFirstSchedulePeriod(pairScheduleId));
                Logger.Info($"Loan status change schedule added for loan {Schedule.LoanReferenceNumber} with status {Schedule.FromStatusCode} to be executed in {Schedule.StartDate}");
                Repository.Add(GetLastSchedulePeriod(pairScheduleId));
                Logger.Info($"Loan status change schedule added for loan {Schedule.LoanReferenceNumber} with status {Schedule.ToStatusCode} to be executed in {Schedule.EndDate}");
            }
            catch (Exception ex) { Logger.Error("Error while saving schedule to database: ", ex); }
        }
    }
}
