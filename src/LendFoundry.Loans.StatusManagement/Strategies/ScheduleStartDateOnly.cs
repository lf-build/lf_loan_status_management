﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.StatusManagement.Events;
using System;

namespace LendFoundry.Loans.StatusManagement
{
    internal class ScheduleStartDateOnly : BaseStrategy, IStatusChangeStrategy
    {
        public ScheduleStartDateOnly
        (
            Schedule schedule,
            ILogger logger,
            IStatusManagementRepository repository,
            IEventHubClient eventHub
        ) : base(schedule, logger, repository, eventHub)
        { }

        public void Execute()
        {
            try
            {
                var schedule = GetFirstSchedulePeriod(GenerateScheduleId());
                Repository.Add(schedule);
                EventHub.Publish(new LoanStatusScheduledAdded
                {
                    ScheduleDetail = schedule
                });
                Logger.Info($"Loan status change schedule added for loan {Schedule.LoanReferenceNumber} with status {Schedule.ToStatusCode} to be executed in {Schedule.StartDate}");
            }
            catch (Exception ex) { Logger.Error("Error while saving schedule to database: ", ex); }
        }
    }
}
