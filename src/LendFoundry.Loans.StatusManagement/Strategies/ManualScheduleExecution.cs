﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.StatusManagement.Events;
using System.Linq;

namespace LendFoundry.Loans.StatusManagement
{
    internal class ManualScheduleExecution : BaseStrategy, IStatusChangeStrategy
    {
        public ManualScheduleExecution
        (
            Schedule schedule,
            ILogger logger,
            IStatusManagementRepository repository,
            IScheduleExecutorFactory executorFactory,
            ILoanService loanService,
            IEventHubClient eventHub
        ) : base(schedule, logger, repository, eventHub, loanService)
        {
            ExecutorFactory = executorFactory;
        }

        private IScheduleExecutorFactory ExecutorFactory { get; }

        public void Execute()
        {
            Logger.Info($"Manual schedule action {Schedule.StateOperation} requested for loan {Schedule.LoanReferenceNumber} and schedule id {Schedule.Id}");

            // Get all scheduled entries.
            var dbSchedule = Repository.All(q =>
                q.ScheduleId == Schedule.Id &&
                q.Status == ScheduleState.Pending).Result.FirstOrDefault();

            if (dbSchedule == null)
                throw new NotFoundException($"Schedule id {Schedule.Id} cannot be found");

            object publishContent = default(object);            
            switch (Schedule.StateOperation)
            {
                case Schedule.State.Execute:
                    ExecutorFactory
                        .Create(LoanService, Logger, Repository, EventHub)
                        .RunIt(dbSchedule);
                    break;
                case Schedule.State.Fail:
                    dbSchedule.Status = ScheduleState.Failed;
                    publishContent = new LoanStatusScheduleFailed { ScheduleDetail = dbSchedule };
                    break;
                case Schedule.State.Cancel:
                    dbSchedule.Status = ScheduleState.Cancelled;
                    publishContent = new LoanStatusScheduleCancelled { ScheduleDetail = dbSchedule };
                    break;
            }
            EventHub.Publish(publishContent);
            Repository.Update(dbSchedule);
            Logger.Info($"Manual schedule execution done");
        }
    }
}
