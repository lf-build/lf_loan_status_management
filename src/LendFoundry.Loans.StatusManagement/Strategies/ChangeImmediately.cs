﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using System;

namespace LendFoundry.Loans.StatusManagement
{
    internal class ChangeImmediately : BaseStrategy, IStatusChangeStrategy
    {
        public ChangeImmediately
        (
            Schedule schedule,
            ILogger logger,
            IStatusManagementRepository repository,
            ILoanService loanService,
            IEventHubClient eventHub
        ) : base(schedule, logger, repository, eventHub, loanService)
        { }

        public void Execute()
        {
            try
            {
                LoanService.UpdateLoanStatus(Schedule.LoanReferenceNumber, Schedule.ToStatusCode);
            }
            catch (Exception ex)
            {
                Logger.Error($"Error while executing {nameof(ChangeImmediately)} process: ", ex);
            };
        }
    }
}
