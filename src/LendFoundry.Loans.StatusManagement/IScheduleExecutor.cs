﻿namespace LendFoundry.Loans.StatusManagement
{
    public interface IScheduleExecutor
    {
        void RunIt(IScheduleDetail schedule);
    }
}
