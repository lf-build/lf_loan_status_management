﻿namespace LendFoundry.Loans.StatusManagement
{
    public interface IStatusChangeStrategy
    {
        void Execute();
    }
}
