﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.StatusManagement.Events;
using System;

namespace LendFoundry.Loans.StatusManagement
{
    public class ScheduleExecutor : IScheduleExecutor
    {
        public ScheduleExecutor
        (
            ILoanService loanService,
            ILogger logger,
            IEventHubClient eventHub
        )
        {
            LoanService = loanService;
            Logger = logger;
            EventHub = eventHub;
        }

        public ScheduleExecutor
        (
            ILoanService loanService,
            ILogger logger,
            IStatusManagementRepository repository,
            IEventHubClient eventHub
        )
        {
            LoanService = loanService;
            Logger = logger;
            Repository = repository;
            EventHub = eventHub;
        }

        private ILoanService LoanService { get; }

        private ILogger Logger { get; }

        private IStatusManagementRepository Repository { get; }

        private IEventHubClient EventHub { get; }

        public void RunIt(IScheduleDetail schedule)
        {
            try
            {
                LoanService.UpdateLoanStatus(schedule.LoanReferenceNumber, schedule.LoanStatusCode);
                Logger.Info($"Request to update loan {schedule.LoanReferenceNumber} to status {schedule.LoanStatusCode} sent");

                schedule.Status = ScheduleState.Executed;
                Logger.Info($"Schedule id {schedule.ScheduleId} executed properly");
            }
            catch (Exception ex)
            {
                schedule.Status = ScheduleState.Failed;
                Logger.Error($"Error while executing schedule id {schedule.ScheduleId}", ex);
            }
            finally
            {
                object publishContent = default(object);
                switch (schedule.Status)
                {
                    case ScheduleState.Executed:
                        publishContent = new LoanStatusScheduledOk { ScheduleDetail = schedule };
                        break;
                    case ScheduleState.Failed:
                        publishContent = new LoanStatusScheduleFailed { ScheduleDetail = schedule };
                        break;
                }
                EventHub.Publish(publishContent);
                Repository?.Update(schedule);
            }
        }
    }
}
