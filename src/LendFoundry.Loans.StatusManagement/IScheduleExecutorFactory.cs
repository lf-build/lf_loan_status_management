﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;

namespace LendFoundry.Loans.StatusManagement
{
    public interface IScheduleExecutorFactory
    {
        IScheduleExecutor Create(ILoanService loanService, ILogger logger, IStatusManagementRepository repository, IEventHubClient eventHub);
        IScheduleExecutor Create(ILoanService loanService, ILogger logger, IEventHubClient eventHub);
    }
}
