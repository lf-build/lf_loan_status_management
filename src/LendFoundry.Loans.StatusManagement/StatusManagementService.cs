﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using System;
using System.Linq;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;

namespace LendFoundry.Loans.StatusManagement
{
    public class StatusManagementService : IStatusManagementService
    {
        public StatusManagementService
        (
            IStatusChangeStrategyFactory strategyFactory,
            ILogger logger,
            ILoanService loanService, 
            ITenantTime tenantTime, 
            LoanConfiguration loanConfiguration)
        {
            StrategyFactory = strategyFactory;
            Logger = logger;
            LoanService = loanService;
            TenantTime = tenantTime;
            LoanConfiguration = loanConfiguration;
        }

        private IStatusChangeStrategyFactory StrategyFactory { get; }

        private LoanConfiguration LoanConfiguration { get; }

        private ILogger Logger { get; }

        private ILoanService LoanService { get; }
        private ITenantTime TenantTime { get; }

        public void ChangeStatus(string loanReferenceNumber, string toStatus, DateTimeOffset? startDate = default(DateTimeOffset?), string fromStatus = "", DateTimeOffset? endDate = default(DateTimeOffset?))
        {
            ValidReferenceNumber(loanReferenceNumber);

            ValidToStatus(toStatus);

            ValidDates(startDate, endDate);

            Logger.Info($"Status change requested for loan {loanReferenceNumber}");
            StrategyFactory
                .Create(loanReferenceNumber, toStatus, startDate, fromStatus, endDate)
                .Execute();
            Logger.Info("Status change operation finished");
        }

        public void ManualScheduleOperation(string loanReferenceNumber, string scheduleId, string action)
        {
            ValidReferenceNumber(loanReferenceNumber);

            if (string.IsNullOrWhiteSpace(scheduleId))
                throw new ArgumentException($"{nameof(scheduleId)} cannot be empty");

            if (string.IsNullOrWhiteSpace(action))
                throw new ArgumentException($"{nameof(action)} cannot be empty");

            Logger.Info($"Manual schedule execution {action} requested for loan {loanReferenceNumber} schedule {scheduleId}");
            StrategyFactory
                .Create(loanReferenceNumber, scheduleId, action)
                .Execute();
            Logger.Info("Manual schedule execution operation finished");
        }

        public bool IsAllowedAction(string loanReferenceNumber, string actionType)
        {
            ValidReferenceNumber(loanReferenceNumber);

            if (string.IsNullOrWhiteSpace(actionType))
                throw new ArgumentException($"{nameof(actionType)} cannot be empty");

            Status.Actions action;
            if(!Enum.TryParse(actionType, out action))
                throw new ArgumentException($"{nameof(actionType)} is not a valid action");

            return LoanService.IsAllowedAction(loanReferenceNumber, @action);            
        }

        private void ValidDates(DateTimeOffset? startDate, DateTimeOffset? endDate)
        {
            var currentDate = TenantTime.Today;

            if (startDate < currentDate)
            {
                throw new InvalidArgumentException($"Start Date : {startDate.Value} is below the current date.");
            }

            if (endDate < currentDate)
            {
                throw new InvalidArgumentException($"End Date : {endDate.Value} is below the current date.");
            }
        }

        private void ValidToStatus(string toStatus)
        {
            if (string.IsNullOrWhiteSpace(toStatus))
                throw new ArgumentException($"{nameof(toStatus)} cannot be empty");

            if (!LoanConfiguration.LoanStatus.Any(a => a.Code.Equals(toStatus)))
            {
                throw new NotFoundException($"Loan Status code: {toStatus} cannot be found.");
            }
            
        }

        private void ValidReferenceNumber(string loanReferenceNumber)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentException($"{nameof(loanReferenceNumber)} cannot be empty");

            try
            {
                var loan = LoanService.GetLoanInfo(loanReferenceNumber);
                if (loan?.Loan == null)
                {
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                throw new NotFoundException($"LoanReferenceNumber : {loanReferenceNumber} it was not found.");
            }
        }
    }
}