﻿using System;

namespace LendFoundry.Loans.StatusManagement
{
    public interface IStatusChangeStrategyFactory
    {
        IStatusChangeStrategy Create(string loanReferenceNumber, string toStatus, DateTimeOffset? startDate = default(DateTimeOffset?), string fromStatus = "", DateTimeOffset? endDate = default(DateTimeOffset?));
        IStatusChangeStrategy Create(string loanReferenceNumber, string scheduleId, string action);
    }
}
