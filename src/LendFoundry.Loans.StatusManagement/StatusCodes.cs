﻿namespace LendFoundry.Loans.StatusManagement
{
    public class StatusCodes
    {
        public static string Delinquent => "100.04";
        public static string PayOff => "300.05";
        public static string InService => "100.03";
    }
}
