﻿using System;

namespace LendFoundry.Loans.StatusManagement
{
    public class Schedule
    {
        public Schedule() { }

        public Schedule(string loanReferenceNumber, string toStatus, DateTimeOffset? startDate = default(DateTimeOffset?), string fromStatus = "", DateTimeOffset? endDate = default(DateTimeOffset?))
        {
            EndDate = endDate;
            FromStatusCode = fromStatus;
            LoanReferenceNumber = loanReferenceNumber;
            StartDate = startDate;
            ToStatusCode = toStatus;
        }

        public Schedule(string loanReferenceNumber, string scheduleId, string action)
        {
            LoanReferenceNumber = loanReferenceNumber;
            Id = scheduleId;

            switch (action.ToLower())
            {
                case "execute": StateOperation = State.Execute; break;
                case "fail": StateOperation = State.Fail; break;
                case "cancel": StateOperation = State.Cancel; break;
                default:
                    throw new ArgumentException($"Action {action} is not a valid schedule operation");
            }
        }

        public DateTimeOffset? EndDate { get; set; }
        public string FromStatusCode { get; set; }
        public string LoanReferenceNumber { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public string ToStatusCode { get; set; }
        public string Id { get; set; }
        public State StateOperation { get; set; }
        public enum State
        {
            Execute,
            Fail,
            Cancel
        }
    }
}
