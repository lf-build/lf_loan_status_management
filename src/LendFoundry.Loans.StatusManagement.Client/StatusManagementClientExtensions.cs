﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.StatusManagement.Client
{
    public static class StatusManagementClientExtensions
    {
        public static IServiceCollection AddStatusManagement(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IStatusManagementClientFactory>(p => new StatusManagementClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IStatusManagementClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
