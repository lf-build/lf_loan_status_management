﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.StatusManagement.Client
{
    public interface IStatusManagementClientFactory
    {
        IStatusManagementService Create(ITokenReader reader);
    }
}
