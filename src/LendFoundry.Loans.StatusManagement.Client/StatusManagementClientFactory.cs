﻿using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Loans.StatusManagement.Client
{
    public class StatusManagementClientFactory : IStatusManagementClientFactory
    {
        public StatusManagementClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; set; }

        private string Endpoint { get; set; }

        private int Port { get; set; }

        public IStatusManagementService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new StatusManagementService(client);
        }
    }
}
