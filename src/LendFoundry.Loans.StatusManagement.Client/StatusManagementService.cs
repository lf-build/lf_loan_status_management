﻿using LendFoundry.Foundation.Services;
using RestSharp;
using System;

namespace LendFoundry.Loans.StatusManagement.Client
{
    public class StatusManagementService : IStatusManagementService
    {
        public StatusManagementService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }
         
        public void ChangeStatus(string loanReferenceNumber, string toStatus, DateTimeOffset? startDate = default(DateTimeOffset?), string fromStatus = "", DateTimeOffset? endDate = default(DateTimeOffset?))
        {
            var request = new RestRequest("/{loanReferenceNumber}/{toStatus}/{startDate?}/{fromStatus?}/{endDate?}", Method.POST);
            request.AddUrlSegment("loanReferenceNumber", loanReferenceNumber);
            request.AddUrlSegment("toStatus", toStatus);

            if(startDate.HasValue)
                request.AddUrlSegment("startDate", startDate.Value.ToString());

            if (startDate.HasValue)
                request.AddUrlSegment("fromStatus", fromStatus);

            if (startDate.HasValue)
                request.AddUrlSegment("endDate", endDate.Value.ToString());

            Client.Execute(request);
        }

        public bool IsAllowedAction(string loanReferenceNumber, string actionType)
        {
            var request = new RestRequest("/{loanReferenceNumber}/is-allowed/{actionType}", Method.GET);
            request.AddUrlSegment("loanReferenceNumber", loanReferenceNumber);
            request.AddUrlSegment("actionType", actionType);
            return Client.Execute<bool>(request);
        }

        public void ManualScheduleOperation(string loanReferenceNumber, string scheduleId, string action)
        {
            //todo: Should not be external            
        }
    }
}
