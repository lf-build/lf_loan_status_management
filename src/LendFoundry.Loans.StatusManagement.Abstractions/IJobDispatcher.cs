﻿namespace LendFoundry.Loans.StatusManagement
{
    public interface IJobDispatcher
    {
        void Start();
    }
}
