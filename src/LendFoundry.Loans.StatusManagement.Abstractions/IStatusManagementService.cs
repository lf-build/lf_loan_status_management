﻿using System;

namespace LendFoundry.Loans.StatusManagement
{
    public interface IStatusManagementService
    {
        void ChangeStatus(string loanReferenceNumber, string toStatus, DateTimeOffset? startDate = default(DateTimeOffset?), string fromStatus = "", DateTimeOffset? endDate = default(DateTimeOffset?));

        void ManualScheduleOperation(string loanReferenceNumber, string scheduleId, string action);

        bool IsAllowedAction(string loanReferenceNumber, string actionType);
    }
}
