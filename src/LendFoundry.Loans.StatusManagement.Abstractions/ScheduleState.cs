﻿namespace LendFoundry.Loans.StatusManagement
{
    public enum ScheduleState
    {
        Pending,
        Executed,
        Failed,
        Cancelled
    }
}
