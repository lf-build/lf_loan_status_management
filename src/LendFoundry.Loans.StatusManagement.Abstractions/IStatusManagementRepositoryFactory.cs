﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Loans.StatusManagement
{
    public interface IStatusManagementRepositoryFactory
    {
        IStatusManagementRepository Create(ITokenReader reader);
    }
}
