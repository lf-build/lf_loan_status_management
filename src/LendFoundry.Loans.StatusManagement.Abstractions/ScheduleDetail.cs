﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans.StatusManagement
{
    public class ScheduleDetail : Aggregate, IScheduleDetail
    {
        public ScheduleDetail()
        {
        }

        public ScheduleDetail(string loanReferenceNumber, string loanStatusCode)
        {
            LoanReferenceNumber = loanReferenceNumber;
            LoanStatusCode = loanStatusCode;
        }

        public TimeBucket Date { get; set; }
        public string LoanReferenceNumber { get; set; }
        public string LoanStatusCode { get; set; }
        public string ScheduleId { get; set; }
        public ScheduleState Status { get; set; }
    }
}
