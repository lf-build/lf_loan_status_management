﻿namespace LendFoundry.Loans.StatusManagement
{
    public class Trigger
    {
        public string Tenant { get; set; }
        public string CronExpression { get; set; }
    }
}
