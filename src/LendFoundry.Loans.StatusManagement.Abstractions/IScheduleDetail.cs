﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans.StatusManagement
{
    public interface IScheduleDetail : IAggregate
    {
        string ScheduleId { get; set; }
        TimeBucket Date { get; set; }
        ScheduleState Status { get; set; }
        string LoanReferenceNumber { get; set; }
        string LoanStatusCode { get; set; }        
    }
}
