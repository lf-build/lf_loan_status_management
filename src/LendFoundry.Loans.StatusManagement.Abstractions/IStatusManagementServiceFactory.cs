﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.StatusManagement
{
    public interface IStatusManagementServiceFactory
    {
        IStatusManagementService Create(ILogger logger);
    }
}
