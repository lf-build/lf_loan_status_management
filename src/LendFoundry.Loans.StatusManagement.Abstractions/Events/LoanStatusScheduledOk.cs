﻿namespace LendFoundry.Loans.StatusManagement.Events
{
    public class LoanStatusScheduledOk
    {
        public IScheduleDetail ScheduleDetail { get; set; }
    }
}
