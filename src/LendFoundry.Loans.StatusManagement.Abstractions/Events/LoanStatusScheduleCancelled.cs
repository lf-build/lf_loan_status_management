﻿namespace LendFoundry.Loans.StatusManagement.Events
{
    public class LoanStatusScheduleCancelled
    {
        public IScheduleDetail ScheduleDetail { get; set; }
    }
}
