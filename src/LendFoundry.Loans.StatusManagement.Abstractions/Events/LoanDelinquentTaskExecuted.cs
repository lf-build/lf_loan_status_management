namespace LendFoundry.Loans.StatusManagement.Events
{
    public class LoanDelinquentTaskExecuted
    {
        public string LoanReferenceNumber { get; set; }
    }
}