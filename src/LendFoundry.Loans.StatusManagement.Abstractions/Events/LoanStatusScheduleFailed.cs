﻿namespace LendFoundry.Loans.StatusManagement.Events
{
    public class LoanStatusScheduleFailed
    {
        public IScheduleDetail ScheduleDetail { get; set; }
    }
}
