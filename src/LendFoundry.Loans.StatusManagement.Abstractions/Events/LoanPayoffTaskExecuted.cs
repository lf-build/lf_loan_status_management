﻿namespace LendFoundry.Loans.StatusManagement.Events
{
    public class LoanPayoffTaskExecuted
    {
        public string LoanReferenceNumber { get; set; }
    }
}