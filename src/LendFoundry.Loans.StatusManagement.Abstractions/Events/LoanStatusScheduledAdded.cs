﻿namespace LendFoundry.Loans.StatusManagement.Events
{
    public class LoanStatusScheduledAdded
    {
        public IScheduleDetail ScheduleDetail { get; set; }
    }
}
