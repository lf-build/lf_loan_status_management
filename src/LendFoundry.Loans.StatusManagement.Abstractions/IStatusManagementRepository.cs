﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Loans.StatusManagement
{
    public interface IStatusManagementRepository : IRepository<IScheduleDetail>
    {
    }
}
