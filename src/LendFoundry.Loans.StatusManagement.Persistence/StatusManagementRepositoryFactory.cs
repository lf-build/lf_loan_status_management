﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.StatusManagement.Persistence
{
    public class StatusManagementRepositoryFactory : IStatusManagementRepositoryFactory
    {
        public StatusManagementRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IStatusManagementRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new StatusManagementRepository(tenantService, mongoConfiguration);            
        }
    }
}
