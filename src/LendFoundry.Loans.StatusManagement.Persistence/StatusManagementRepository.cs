﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace LendFoundry.Loans.StatusManagement.Persistence
{
    public class StatusManagementRepository : MongoRepository<IScheduleDetail, ScheduleDetail>, IStatusManagementRepository
    {
        static StatusManagementRepository()
        {
            BsonClassMap.RegisterClassMap<ScheduleDetail>(map =>
            {
                map.AutoMap();
                map.MapMember(f => f.Status).SetSerializer(new EnumSerializer<ScheduleState>(BsonType.String));

                var type = typeof(ScheduleDetail);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public StatusManagementRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "status-management")
        {            
            CreateIndexIfNotExists
            (
               indexName: "loan-reference-number",
               index: Builders<IScheduleDetail>.IndexKeys.Ascending(i => i.LoanReferenceNumber)
            );

            CreateIndexIfNotExists
            (
               indexName: "status-code",
               index: Builders<IScheduleDetail>.IndexKeys.Ascending(i => i.LoanStatusCode)
            );

            CreateIndexIfNotExists
            (
               indexName: "schedule-id",
               index: Builders<IScheduleDetail>.IndexKeys.Ascending(i => i.ScheduleId)
            );
        }
    }
}
