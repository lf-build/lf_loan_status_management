﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using System;

namespace LendFoundry.Loans.StatusManagement.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(ILogger logger, IStatusManagementService service)
            : base(logger)
        {
            Service = service;
        }

        private IStatusManagementService Service { get; }

        [HttpPost("/{loanReferenceNumber}/{toStatus}/{startDate?}/{fromStatus?}/{endDate?}")]
        public IActionResult ChangeStatus(string loanReferenceNumber, string toStatus,
                                          DateTimeOffset? startDate = default(DateTimeOffset?),
                                          string fromStatus = "", DateTimeOffset? endDate = default(DateTimeOffset?)
        )
        {
            return Execute(() =>
            {
                Service.ChangeStatus(loanReferenceNumber, toStatus, startDate, fromStatus, endDate);
                return new HttpStatusCodeResult(204);
            });
        }

        [HttpPost("/{loanReferenceNumber}/manual/{scheduleId}/{operation}")]
        public IActionResult ManualScheduleOperation(string loanReferenceNumber, string scheduleId, string operation)
        {
            return Execute(() =>
            {
                Service.ManualScheduleOperation(loanReferenceNumber, scheduleId, operation);
                return new HttpStatusCodeResult(204);
            });
        }

        [HttpGet("/{loanReferenceNumber}/is-allowed/{actionType}")]
        public IActionResult IsAllowedAction(string loanReferenceNumber, string actionType)
        {
            return Execute(() =>
            {
                var isAllowed = Service.IsAllowedAction(loanReferenceNumber, actionType);                
                return new HttpStatusCodeResult(isAllowed ? 204 : 403);
            });
        }
    }
}