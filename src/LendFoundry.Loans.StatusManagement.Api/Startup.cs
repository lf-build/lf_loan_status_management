﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.StatusManagement.Persistence;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.StatusManagement.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {            
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddConfigurationService<ConfigurationKeys>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddConfigurationService<LoanConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, "loan");
            services.AddTransient(provider =>
            {
                return provider
                    .GetService<IConfigurationService<LoanConfiguration>>()
                    .Get();
            });
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddLoanService(Settings.Loan.Host, Settings.Loan.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.ServiceName);

            // interface implements
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IStatusManagementRepository, StatusManagementRepository>();
            services.AddTransient<IStatusManagementRepositoryFactory, StatusManagementRepositoryFactory>();
            services.AddTransient<IStatusManagementService, StatusManagementService>();
            services.AddTransient<IStatusManagementServiceFactory, StatusManagementServiceFactory>();
            services.AddTransient<IStatusChangeStrategyFactory, StatusChangeStrategyFactory>();
            services.AddTransient<IJobDispatcher, JobDispatcher>();
            services.AddTransient<IScheduleExecutorFactory, ScheduleExecutorFactory>();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseJobDispacher();      
            app.UseMvc();
            app.UseEventHub();
        }
    }
}
