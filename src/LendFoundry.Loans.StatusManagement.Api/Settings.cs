﻿using LendFoundry.Foundation.Services.Settings;
using System;


namespace LendFoundry.Loans.StatusManagement.Api
{
    public class Settings
    {
        private const string Prefix = "STATUS_MANAGEMENT";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings Loan { get;} = new ServiceSettings($"{Prefix}_LOAN", "loan");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "status-management");

        public static string ServiceName => Environment.GetEnvironmentVariable($"{Prefix}_TOKEN_ISSUER") ?? "status-management";
    }
}