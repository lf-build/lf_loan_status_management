﻿using LendFoundry.Loans.StatusManagement.Events;
using Moq;
using System;
using Xunit;

namespace LendFoundry.Loans.StatusManagement.Tests
{
    public class ScheduleExecutorTest : TestObjects
    {
        IScheduleExecutor ScheduleExecutor
        {
            get
            {
                return new ScheduleExecutor
                (
                    MockCollection.LoanService.Object, 
                    MockCollection.Logger.Object, 
                    MockCollection.Reposiory.Object, 
                    MockCollection.Eventhub.Object
                );
            }
        }

        IScheduleExecutor ScheduleExecutorNoRepository
        {
            get
            {
                return new ScheduleExecutor
                (
                    MockCollection.LoanService.Object,
                    MockCollection.Logger.Object, 
                    null,
                    MockCollection.Eventhub.Object
                );
            }
        }

        [Fact]
        public void ScheduleExecutor_WhenProcessExecuted()
        {
            var loanService = MockCollection.LoanService;
            var eventhub = MockCollection.Eventhub;
            var repository = MockCollection.Reposiory;
            var logger = MockCollection.Logger;
            var schedule = ObjectCollection.ScheduleDetail;
            loanService.Setup(s => s.UpdateLoanStatus(schedule.LoanReferenceNumber, schedule.LoanStatusCode));
            eventhub.Setup(s => s.Publish(It.IsAny<LoanStatusScheduledOk>()));
            repository.Setup(s => s.Update(It.IsAny<IScheduleDetail>()));

            ScheduleExecutor.RunIt(schedule);

            Assert.True(schedule.Status == ScheduleState.Executed);
            loanService.Verify(x => x.UpdateLoanStatus(schedule.LoanReferenceNumber, schedule.LoanStatusCode));
            eventhub.Verify(x => x.Publish<Object>(It.IsAny<LoanStatusScheduledOk>()));
            repository.Verify(x => x.Update(It.IsAny<IScheduleDetail>()));
            logger.Verify(x => x.Info(It.IsAny<string>()));
        }

        [Fact]
        public void ScheduleExecutor_WhenProcessFailed()
        {
            var loanService = MockCollection.LoanService;
            var eventhub = MockCollection.Eventhub;
            var repository = MockCollection.Reposiory;
            var logger = MockCollection.Logger;
            var schedule = ObjectCollection.ScheduleDetail;
            loanService.Setup(s => s.UpdateLoanStatus(schedule.LoanReferenceNumber, schedule.LoanStatusCode)).Throws<Exception>();
            eventhub.Setup(s => s.Publish(It.IsAny<LoanStatusScheduleFailed>()));
            repository.Setup(s => s.Update(It.IsAny<IScheduleDetail>()));

            ScheduleExecutor.RunIt(schedule);

            Assert.True(schedule.Status == ScheduleState.Failed);
            eventhub.Verify(x => x.Publish<Object>(It.IsAny<LoanStatusScheduleFailed>()));
            repository.Verify(x => x.Update(It.IsAny<IScheduleDetail>()));
        }

        [Fact]
        public void ScheduleExecutor_WhenNoRepositoryInformed()
        {
            var loanService = MockCollection.LoanService;
            var eventhub = MockCollection.Eventhub;
            var repository = MockCollection.Reposiory;
            var logger = MockCollection.Logger;
            var schedule = ObjectCollection.ScheduleDetail;
            loanService.Setup(s => s.UpdateLoanStatus(schedule.LoanReferenceNumber, schedule.LoanStatusCode));
            eventhub.Setup(s => s.Publish(It.IsAny<LoanStatusScheduleFailed>()));
            repository.Setup(s => s.Update(It.IsAny<IScheduleDetail>()));

            ScheduleExecutorNoRepository.RunIt(schedule);

            repository.Verify(x => x.Update(It.IsAny<IScheduleDetail>()), Times.Never);
        }
    }
}
