﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using System;

namespace LendFoundry.Loans.StatusManagement.Tests
{
    public class TestObjects
    {
        public static class MockCollection
        {
            public static Mock<ILoanService> LoanService = new Mock<ILoanService>();
            public static Mock<ILogger> Logger = new Mock<ILogger>();
            public static Mock<IStatusManagementRepository> Reposiory = new Mock<IStatusManagementRepository>();
            public static Mock<IEventHubClient> Eventhub = new Mock<IEventHubClient>();
            public static Mock<IStatusChangeStrategyFactory> StrategyFactory = new Mock<IStatusChangeStrategyFactory>();
            public static Mock<ITenantServiceFactory> TenantServiceFactory = new Mock<ITenantServiceFactory>();
            public static Mock<ITokenHandler> TokenHandler = new Mock<ITokenHandler>();
            public static Mock<ITenantTimeFactory> TenantTimeFactory = new Mock<ITenantTimeFactory>();
            public static Mock<IConfigurationServiceFactory> ConfigurationFactory = new Mock<IConfigurationServiceFactory>();
            public static Mock<IToken> Token = new Mock<IToken>();
            public static Mock<ITenantTime> TenantTime = new Mock<ITenantTime>();
        }

        public static class ObjectCollection
        {
            public static IScheduleDetail ScheduleDetail
            {
                get
                {
                    return new ScheduleDetail
                    {
                        Id = Guid.NewGuid().ToString("N"),
                        Date = new Foundation.Date.TimeBucket(DateTimeOffset.MinValue),
                        LoanReferenceNumber = "ln0001",
                        LoanStatusCode = "100.03",
                        ScheduleId = Guid.NewGuid().ToString("N"),
                        Status = ScheduleState.Pending,
                        TenantId = "my-tenant"
                    };
                }
            }

            public static TenantInfo[] ListTenantInfo
            {
                get
                {
                    return new[]
                    {
                        new TenantInfo{ Id = "my-tenant1" },
                        new TenantInfo{ Id = "my-tenant2" },
                    };
                }
            }

            public static Schedule Schedule
            {
                get
                {
                    return new Schedule();
                }
            }
        }
    }
}
