﻿using Moq;
using System;
using Xunit;

namespace LendFoundry.Loans.StatusManagement.Tests
{
    public class StatusManagementServiceTest : TestObjects
    {
        IStatusManagementService StatusManagementService
        {
            get
            {
                return new StatusManagementService
                (
                    MockCollection.StrategyFactory.Object,
                    MockCollection.Logger.Object,
                    MockCollection.LoanService.Object,
                    MockCollection.TenantTime.Object,
                    Mock.Of<LoanConfiguration>());
            }
        }

        [Fact]
        public void ChangeStatus_WhenDoesNotHaveLoanReference()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusManagementService.ChangeStatus(string.Empty, "100.03");
            });
        }

        [Fact]
        public void ChangeStatus_WhenDoesNotHaveToStatus()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusManagementService.ChangeStatus("ln001", string.Empty);
            });
        }

        [Fact]
        public void ChangeStatus_WhenExecutionOk()
        {
            var strategy = MockCollection.StrategyFactory;
            var logger = MockCollection.Logger;
            strategy.Setup(s => s.Create("", "", DateTimeOffset.MinValue, "", DateTimeOffset.MinValue).Execute());
            var service = StatusManagementService;

            service.ChangeStatus("ln001", "100.04");

            logger.Verify(x => x.Info(It.IsAny<string>()));
            strategy.Verify(x => x.Create("", "", DateTimeOffset.MinValue, "", DateTimeOffset.MinValue).Execute());
        }

        [Fact]
        public void ManualScheduleOperation_WhenExecutionOk()
        {
            var logger = MockCollection.Logger;
            var strategy = MockCollection.StrategyFactory;
            strategy.Setup(s => s.Create("", "", "execute").Execute());
            var service = StatusManagementService;

            service.ManualScheduleOperation("ln001", "123456", "execute");

            logger.Verify(x => x.Info(It.IsAny<string>()));
            strategy.Verify(x => x.Create("", "", "execute").Execute());
        }

        [Fact]
        public void ManualScheduleOperation_WhenHasNoLoanReference()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusManagementService.ManualScheduleOperation(string.Empty, "123456", "execute");
            });
        }

        [Fact]
        public void ManualScheduleOperation_WhenHasNoScheduleId()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusManagementService.ManualScheduleOperation("123", string.Empty, "execute");
            });
        }

        [Fact]
        public void ManualScheduleOperation_WhenHasNoAction()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusManagementService.ManualScheduleOperation("123", "123", string.Empty);
            });
        }

        [Fact]
        public void IsAllowedAction_WhenHasExecutionOk()
        {
            var loanService = MockCollection.LoanService;
            loanService.Setup(s => s.IsAllowedAction("123", Status.Actions.AccrueInterest)).Returns(true);
            var service = StatusManagementService;

            service.IsAllowedAction("123", "AccrueInterest");

            loanService.Verify(x => x.IsAllowedAction(It.IsAny<string>(), Status.Actions.AccrueInterest));
        }

        [Fact]
        public void IsAllowedAction_WhenHasNoLoanReference()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusManagementService.IsAllowedAction("", "123");
            });
        }

        [Fact]
        public void IsAllowedAction_WhenHasNoActionType()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusManagementService.IsAllowedAction("123", "");
            });
        }

        [Fact]
        public void IsAllowedAction_WhenActionCannotBeResolved()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusManagementService.IsAllowedAction("123", "SomeInvalidAction");
            });
        }
    }
}
