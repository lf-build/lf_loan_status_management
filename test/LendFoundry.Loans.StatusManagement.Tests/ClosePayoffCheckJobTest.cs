﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.StatusManagement.Jobs;
using LendFoundry.Security.Tokens;
using Moq;
using Quartz;
using System;
using Xunit;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.StatusManagement.Tests
{
    //todo: Implement unit test for the module ClosePayoffCheckJobTest
    public class ClosePayoffCheckJobTest : TestObjects
    {
        Mock<IJobExecutionContext> Context = new Mock<IJobExecutionContext>();
        ClosePayoffCheckJob Job
        {
            get { return new ClosePayoffCheckJob(); }
        }

        [Fact]
        public void Execute_WhenAllProcessOk()
        {            
            var logger = MockCollection.Logger;
            var repository = MockCollection.Reposiory;
            var loanService = MockCollection.LoanService;
            var tenantTime = MockCollection.TenantTime;

            var serviceProvider = new Mock<IServiceProvider>();
            serviceProvider.Setup(s => s.GetService<ILoggerFactory>().CreateLogger()).Returns(logger.Object);
            serviceProvider.Setup(s => s.GetService<IStatusManagementRepositoryFactory>().Create(It.IsAny<StaticTokenReader>())).Returns(repository.Object);
            serviceProvider.Setup(s => s.GetService<ILoanServiceFactory>().Create(It.IsAny<StaticTokenReader>())).Returns(loanService.Object);
            serviceProvider.Setup(s => s.GetService<IScheduleServiceFactory>().Create(It.IsAny<StaticTokenReader>())).Returns(Mock.Of<IScheduleService>());
            serviceProvider.Setup(s => s.GetService<IConfigurationServiceFactory>());
            serviceProvider.Setup(s => s.GetService<ITenantTimeFactory>().Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<StaticTokenReader>())).Returns(tenantTime.Object);
            serviceProvider.Setup(s => s.GetService<IScheduleExecutorFactory>());
            serviceProvider.Setup(s => s.GetService<IEventHubClient>());

            Context.Setup(s => s.MergedJobDataMap.Get("provider")).Returns(serviceProvider.Object);
            Context.Setup(s => s.MergedJobDataMap.Get("tenantReader")).Returns(It.IsAny<StaticTokenReader>());

            Job.Execute(Context.Object);
        }
    }
}