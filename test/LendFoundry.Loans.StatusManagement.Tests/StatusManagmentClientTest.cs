﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Loans.StatusManagement.Tests
{
    public class StatusManagmentClientTest
    {
        IServiceClient GetServiceClient()
        {
            // Host
            string host = "localhost";
            int port = 5000;

            // Token generation
            var tokenHandler = new TokenHandler(null, null, null);
            var token = tokenHandler.Issue("my-tenant", "some-app");
            var reader = new StaticTokenReader(token.Value);

            // Service client generation
            return new ServiceClient(
                accessor: new EmptyHttpContextAccessor(),
                logger: Mock.Of<ILogger>(),
                tokenReader: reader,
                endpoint: host,
                port: port
            );
        }

        [Fact]
        public void ExecuteClient_ChangeStatus_WhenNoDates()
        {
            new Client.StatusManagementService(GetServiceClient())
                .ChangeStatus("ln001", "100.03");
        }

        [Fact]
        public void ExecuteClient_ChangeStatus_WhenStartDateOnly()
        {
            new Client.StatusManagementService(GetServiceClient())
                .ChangeStatus("ln001", "100.03", DateTimeOffset.Now.AddDays(1));
        }

        [Fact]
        public void ExecuteClient_ChangeStatus_WhenStartAndEndDate()
        {
            new Client.StatusManagementService(GetServiceClient())
                    .ChangeStatus("ln001", "100.03", DateTimeOffset.Now.AddDays(1), "100.04", DateTimeOffset.Now.AddDays(2));
        }

        [Fact]
        public void ExecuteClient_IsAllowedAction()
        {
            new Client.StatusManagementService(GetServiceClient())
                .IsAllowedAction("ln001", "AutoACH");
        }
    }
}
