﻿using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.StatusManagement.Tests
{
    public class JobDispatcherTest : TestObjects
    {
        IJobDispatcher JobDispatcher
        {
            get
            {
                return new JobDispatcher
                (
                    MockCollection.Logger.Object, 
                    MockCollection.TenantServiceFactory.Object,
                    MockCollection.TokenHandler.Object, 
                    Mock.Of<IServiceProvider>(), 
                    MockCollection.TenantTimeFactory.Object,
                    MockCollection.ConfigurationFactory.Object
                );
            }
        }

        [Fact]
        public void JobDispatcher_WhenStartAttachActiveTenants()
        {
            var token = MockCollection.Token;
            var logger = MockCollection.Logger;
            var tenantServiceFactory = MockCollection.TenantServiceFactory;
            var tokenHandler = MockCollection.TokenHandler;
            var tenantTimeFactory = MockCollection.TenantTimeFactory;
            var configurationFactory = MockCollection.ConfigurationFactory;
            var tenantService = new Mock<ITenantService>();
            var tenantInfoList = ObjectCollection.ListTenantInfo.ToList();
            var tenantTime = MockCollection.TenantTime;
            var configuratonKeys = new ConfigurationKeys
            {
                Triggers = (from a in ObjectCollection.ListTenantInfo
                            select new Trigger
                            {
                                Tenant = a.Id,
                                CronExpression = "0 0/1 * 1/1 * ? *"
                            }).ToArray()
            };

            tenantServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(tenantService.Object);
            tenantService.Setup(s => s.GetActiveTenants()).Returns(tenantInfoList);
            token.Setup(s => s.Value).Returns("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM");
            tokenHandler.Setup(s => s.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(token.Object);
            configurationFactory.Setup(s => s.Create<ConfigurationKeys>(It.IsAny<string>(), It.IsAny<StaticTokenReader>()).Get()).Returns(configuratonKeys);
            tenantTime.Setup(s => s.TimeZone).Returns(TimeZoneInfo.Local);
            tenantTimeFactory.Setup(s => s.Create(configurationFactory.Object, It.IsAny<StaticTokenReader>())).Returns(tenantTime.Object);

            JobDispatcher.Start();

            tenantServiceFactory.Verify(s => s.Create(It.IsAny<StaticTokenReader>()));
            tokenHandler.Verify(s => s.Issue(It.IsAny<string>(), It.IsAny<string>()));
            logger.Verify(x => x.Info(It.IsAny<string>()));
        }

        [Fact]
        public void JobDispatcher_WhenHasNoConfiguration()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var token = MockCollection.Token;
                var logger = MockCollection.Logger;
                var tenantServiceFactory = MockCollection.TenantServiceFactory;
                var tokenHandler = MockCollection.TokenHandler;
                var tenantTimeFactory = MockCollection.TenantTimeFactory;
                var configurationFactory = MockCollection.ConfigurationFactory;
                var tenantService = new Mock<ITenantService>();
                var tenantInfoList = ObjectCollection.ListTenantInfo.ToList();
                var tenantTime = MockCollection.TenantTime;

                tenantServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(tenantService.Object);
                tenantService.Setup(s => s.GetActiveTenants()).Returns(tenantInfoList);
                token.Setup(s => s.Value).Returns("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM");
                tokenHandler.Setup(s => s.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(token.Object);
                configurationFactory.Setup(s => s.Create<ConfigurationKeys>(It.IsAny<string>(), It.IsAny<StaticTokenReader>()).Get()).Returns(It.IsAny<ConfigurationKeys>());
                tenantTime.Setup(s => s.TimeZone).Returns(TimeZoneInfo.Local);
                tenantTimeFactory.Setup(s => s.Create(configurationFactory.Object, It.IsAny<StaticTokenReader>())).Returns(tenantTime.Object);

                JobDispatcher.Start();
            });
        }

        [Fact]
        public void JobDispatcher_WhenHasNoConfigurationTriggers()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var token = MockCollection.Token;
                var logger = MockCollection.Logger;
                var tenantServiceFactory = MockCollection.TenantServiceFactory;
                var tokenHandler = MockCollection.TokenHandler;
                var tenantTimeFactory = MockCollection.TenantTimeFactory;
                var configurationFactory = MockCollection.ConfigurationFactory;
                var tenantService = new Mock<ITenantService>();
                var tenantInfoList = ObjectCollection.ListTenantInfo.ToList();
                var tenantTime = MockCollection.TenantTime;
                var configuratonKeys = new ConfigurationKeys
                {
                    Triggers = null
                };

                tenantServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(tenantService.Object);
                tenantService.Setup(s => s.GetActiveTenants()).Returns(tenantInfoList);
                token.Setup(s => s.Value).Returns("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM");
                tokenHandler.Setup(s => s.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(token.Object);
                configurationFactory.Setup(s => s.Create<ConfigurationKeys>(It.IsAny<string>(), It.IsAny<StaticTokenReader>()).Get()).Returns(configuratonKeys);
                tenantTime.Setup(s => s.TimeZone).Returns(TimeZoneInfo.Local);
                tenantTimeFactory.Setup(s => s.Create(configurationFactory.Object, It.IsAny<StaticTokenReader>())).Returns(tenantTime.Object);

                JobDispatcher.Start();
            });
        }
    }
}
