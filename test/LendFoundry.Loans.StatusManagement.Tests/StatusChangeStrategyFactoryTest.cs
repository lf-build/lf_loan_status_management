﻿using Moq;
using System;
using Xunit;

namespace LendFoundry.Loans.StatusManagement.Tests
{
    public class StatusChangeStrategyFactoryTest : TestObjects
    {
        IStatusChangeStrategyFactory StatusChangeStrategyFactory
        {
            get { return new StatusChangeStrategyFactory(Mock.Of<IServiceProvider>()); }
        }

        [Fact]
        public void CreateStrategy_WhenHasNoLoanReference()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusChangeStrategyFactory
                    .Create("", "123", DateTimeOffset.Now, "123", DateTimeOffset.Now);
            });
        }

        [Fact]
        public void CreateStrategy_WhenHasNoNextStatus()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusChangeStrategyFactory
                    .Create("123", "", DateTimeOffset.Now, "123", DateTimeOffset.Now);
            });
        }

        [Fact]
        public void CreateStrategy_WhenInstantExecutionCanBeResolved()
        {
            var instance = StatusChangeStrategyFactory
                                .Create("ln001", "123456789", "execute");

            Assert.NotNull(instance);
            Assert.Contains("ManualScheduleExecution", instance.GetType().Name);
        }

        [Fact]
        public void CreateStrategy_WhenInstantExecutionHasNoLoanReference()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusChangeStrategyFactory
                    .Create("", "123", "execute");
            });
        }

        [Fact]
        public void CreateStrategy_WhenInstantExecutionHasNoScheduleId()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusChangeStrategyFactory
                    .Create("123", "", "execute");
            });
        }

        [Fact]
        public void CreateStrategy_WhenInstantExecutionHasNoAction()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusChangeStrategyFactory
                    .Create("123", "123", "");
            });
        }

        [Fact]
        public void CreateStrategy_WhenScheduleExecutionCanBeResolvedForChangeImmediately()
        {
            var instance = StatusChangeStrategyFactory
                                .Create("123", "123", null, null);

            Assert.NotNull(instance);
            Assert.Contains("ChangeImmediately", instance.GetType().Name);
        }

        [Fact]
        public void CreateStrategy_WhenScheduleExecutionCanBeResolvedForScheduleWithExpireDate()
        {
            var instance = StatusChangeStrategyFactory
                                .Create("123", "123", DateTimeOffset.Now, string.Empty, DateTimeOffset.Now);

            Assert.NotNull(instance);
            Assert.Contains("ScheduleWithExpireDate", instance.GetType().Name);
        }

        [Fact]
        public void CreateStrategy_WhenScheduleExecutionCanBeResolvedForScheduleStartDateOnly()
        {
            var instance = StatusChangeStrategyFactory
                                .Create("123", "123", DateTimeOffset.Now);

            Assert.NotNull(instance);
            Assert.Contains("ScheduleStartDateOnly", instance.GetType().Name);
        }

        [Fact]
        public void CreateStrategy_WhenManualExecutionHasInvalidAction()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                StatusChangeStrategyFactory
                    .Create("123", "123", "some-invalid-action");
            });
        }
    }
}
