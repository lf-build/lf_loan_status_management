FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Loans.StatusManagement.Abstractions /app/LendFoundry.Loans.StatusManagement.Abstractions
WORKDIR /app/LendFoundry.Loans.StatusManagement.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.StatusManagement.Persistence /app/LendFoundry.Loans.StatusManagement.Persistence
WORKDIR /app/LendFoundry.Loans.StatusManagement.Persistence
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.StatusManagement /app/LendFoundry.Loans.StatusManagement
WORKDIR /app/LendFoundry.Loans.StatusManagement
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.StatusManagement.Client /app/LendFoundry.Loans.StatusManagement.Client
WORKDIR /app/LendFoundry.Loans.StatusManagement.Client
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.StatusManagement.Api /app/LendFoundry.Loans.StatusManagement.Api
WORKDIR /app/LendFoundry.Loans.StatusManagement.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel